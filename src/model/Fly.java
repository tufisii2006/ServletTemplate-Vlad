package model;

import java.util.List;
import java.util.UUID;

import javax.persistence.*;

@Entity
@Table(name="fly")
public class Fly {

	@Id
	@Column
	private String flightNumber;
	
	@Column
	private String airplane;
	
	@ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name = "userfly", joinColumns = @JoinColumn(name = "flyid"), inverseJoinColumns = @JoinColumn(name = "usrid"))
    private List<User> pasagers ;

	@OneToOne(fetch=FetchType.EAGER)
	private City depCity;
	
	@OneToOne(fetch=FetchType.EAGER)
	private City arrivCity;
	
	@Column
	private String arivDate;
	

	
	@Column
	private String depDate;

	public List<User> getPasagers() {
		return pasagers;
	}

	public void setPasagers(List<User> pasagers) {
		this.pasagers = pasagers;
	}



	public Fly() {
		if (flightNumber == null) {
			flightNumber = UUID.randomUUID().toString();
		}
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getAirplane() {
		return airplane;
	}

	public void setAirplane(String airplane) {
		this.airplane = airplane;
	}

	public City getDepCity() {
		return depCity;
	}

	public void setDepCity(City depCity) {
		this.depCity = depCity;
	}

	public City getArrivCity() {
		return arrivCity;
	}

	public void setArrivCity(City arrivCity) {
		this.arrivCity = arrivCity;
	}

	public String getArivDate() {
		return arivDate;
	}

	public void setArivDate(String arivDate) {
		this.arivDate = arivDate;
	}

	public String getDepDate() {
		return depDate;
	}

	public void setDepDate(String depDate) {
		this.depDate = depDate;
	}
	
	
}

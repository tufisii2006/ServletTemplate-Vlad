package accessDB;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import model.*;

public class EntityDatabaseAccess {

	public boolean insertFly(Fly fly) {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("JPAservlet");
		EntityManager eniEntityManager = entityManagerFactory.createEntityManager();
		eniEntityManager.getTransaction().begin();
		eniEntityManager.persist(fly);
		eniEntityManager.getTransaction().commit();
		eniEntityManager.close();
		entityManagerFactory.close();
		return true;
	}

	public boolean updateFly(Fly fly) {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("JPAservlet");
		EntityManager eniEntityManager = entityManagerFactory.createEntityManager();
		eniEntityManager.getTransaction().begin();
		eniEntityManager.merge(fly);
		eniEntityManager.getTransaction().commit();
		eniEntityManager.close();
		entityManagerFactory.close();
		return true;
	}

	public void deleteFly(Fly fly) {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("JPAservlet");
		EntityManager eniEntityManager = entityManagerFactory.createEntityManager();
		eniEntityManager.getTransaction().begin();
		eniEntityManager.remove(eniEntityManager.contains(fly) ? fly : eniEntityManager.merge(fly));
		eniEntityManager.getTransaction().commit();
		eniEntityManager.close();
		entityManagerFactory.close();
	}

	public Fly readFly(String id) {
		Fly fly = new Fly();
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("JPAservlet");
		EntityManager eniEntityManager = entityManagerFactory.createEntityManager();
		eniEntityManager.getTransaction().begin();
		fly = eniEntityManager.find(Fly.class, id);
		eniEntityManager.getTransaction().commit();
		eniEntityManager.close();
		entityManagerFactory.close();
		return fly;
	}

	public List<Fly> viewAllFlys() {
		List<Fly> list = new ArrayList<Fly>();
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("JPAservlet");
		EntityManager eniEntityManager = entityManagerFactory.createEntityManager();
		eniEntityManager.getTransaction().begin();
		list = eniEntityManager.createQuery("SELECT a FROM Fly a", Fly.class).getResultList();
		eniEntityManager.getTransaction().commit();
		eniEntityManager.close();
		entityManagerFactory.close();
		return list;
	}

	public Fly getFlyByGivenName(String name) {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("JPAservlet");
		EntityManager eniEntityManager = entityManagerFactory.createEntityManager();
		eniEntityManager.getTransaction().begin();
		Query q = eniEntityManager.createQuery("SELECT a FROM Fly a WHERE a.name = " + "'" + name + "'");
		Fly fly = (Fly) q.getSingleResult();
		eniEntityManager.getTransaction().commit();
		eniEntityManager.close();
		entityManagerFactory.close();
		return fly;
	}

	public City getCityByLocation(String location) {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("JPAservlet");
		EntityManager eniEntityManager = entityManagerFactory.createEntityManager();
		eniEntityManager.getTransaction().begin();
		Query q = eniEntityManager.createQuery("SELECT a FROM City a WHERE a.location = " + "'" + location + "'");
		City fly = (City) q.getSingleResult();
		eniEntityManager.getTransaction().commit();
		eniEntityManager.close();
		entityManagerFactory.close();
		return fly;
	}

	public User getUserByEmailAndPassword(String email, String password) {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("JPAservlet");
		EntityManager eniEntityManager = entityManagerFactory.createEntityManager();
		eniEntityManager.getTransaction().begin();
		Query q = eniEntityManager.createQuery("SELECT a FROM User a WHERE a.email =:email and a.passw=:password ");
		q.setParameter("email", email);
		q.setParameter("password", password);
		User user = (User) q.getSingleResult();
		eniEntityManager.getTransaction().commit();
		eniEntityManager.close();
		entityManagerFactory.close();
		return user;
	}

}
